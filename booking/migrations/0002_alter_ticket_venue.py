# Generated by Django 3.2.12 on 2023-02-26 22:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('booking', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ticket',
            name='venue',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='booking.venue'),
        ),
    ]
